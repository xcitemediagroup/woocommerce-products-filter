<?php
if (!defined('ABSPATH'))
    die('No direct access allowed');

final class WOOF_EXT_BY_SKU extends WOOF_EXT
{

    public $type = 'by_html_type';
    public $html_type = 'by_sku'; //your custom key here
    public $index = 'woof_sku';
    public $html_type_dynamic_recount_behavior = null;

    public function __construct()
    {
        parent::__construct();
        $this->init();
    }

    public function get_ext_path()
    {
        return plugin_dir_path(__FILE__);
    }

    public function get_ext_link()
    {
        return plugin_dir_url(__FILE__);
    }

    public function woof_add_items_keys($keys)
    {
        $keys[] = $this->html_type;
        return $keys;
    }

    public function init()
    {
        if ((int) get_option('woof_first_init', 0) != 1)
        {
            update_option('woof_sku_conditions', 'LIKE');
        }

        add_filter('woof_add_items_keys', array($this, 'woof_add_items_keys'));
        add_action('woof_print_html_type_options_' . $this->html_type, array($this, 'woof_print_html_type_options'), 10, 1);
        add_action('woof_print_html_type_' . $this->html_type, array($this, 'print_html_type'), 10, 1);
        add_action('wp_head', array($this, 'wp_head'), 1);

        self::$includes['js']['woof_' . $this->html_type . '_html_items'] = $this->get_ext_link() . 'js/' . $this->html_type . '.js';
        self::$includes['css']['woof_' . $this->html_type . '_html_items'] = $this->get_ext_link() . 'css/' . $this->html_type . '.css';
        self::$includes['js_init_functions'][$this->html_type] = 'woof_init_sku';
        //***
        add_shortcode('woof_sku_filter', array($this, 'woof_sku_filter'));
    }

    public function wp_head()
    {
        global $WOOF;
        ?>
        <style type="text/css">
        <?php
        if (isset($WOOF->settings['by_sku']['image']))
        {
            if (!empty($WOOF->settings['by_sku']['image']))
            {
                ?>
                    .woof_sku_search_container .woof_sku_search_go{
                        background: url(<?php echo $WOOF->settings['by_sku']['image'] ?>) !important;
                    }
                <?php
            }
        }
        ?>
        </style>
        <script type="text/javascript">
            if (typeof woof_lang_custom == 'undefined') {
                var woof_lang_custom = {};//!!important
            }
            woof_lang_custom.<?php echo $this->index ?> = "<?php _e('by SKU', 'woocommerce-products-filter') ?>";
        </script>
        <?php
    }

    //shortcode
    public function woof_sku_filter($args = array())
    {
        global $WOOF;
        return $WOOF->render_html($this->get_ext_path() . 'views/shortcodes/woof_sku_filter.php', $args);
    }

    //settings page hook
    public function woof_print_html_type_options()
    {
        global $WOOF;
        echo $WOOF->render_html($this->get_ext_path() . 'views/options.php', array(
            'key' => $this->html_type,
            "woof_settings" => get_option('woof_settings', array())
                )
        );
    }

    public function assemble_query_params(&$meta_query)
    {
        global $WOOF;
        $request = $WOOF->get_request_data();
        if (isset($request['woof_sku']))
        {
            if (!empty($request['woof_sku']))
            {
                $value = $request['woof_sku'];
                /*
                  //for variable products
                  $var_divider = '-';
                  if (strpos($value, $var_divider) !== false)
                  {
                  $value = explode($var_divider, $value);
                  $last = (int) $value[count($value) - 1];
                  if ($last > 0)
                  {
                  unset($value[count($value) - 1]);
                  $value = implode($var_divider, $value);
                  }
                  }
                 */
                //***

                $meta_query[] = array(
                    'key' => '_sku',
                    'value' => $value,
                    'compare' => $WOOF->settings['by_sku']['logic']
                );
            }
        }

        return $meta_query;
    }

}

WOOF_EXT::$includes['html_type_objects']['by_sku'] = new WOOF_EXT_BY_SKU();
